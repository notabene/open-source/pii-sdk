/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeAll, expect, test } from 'vitest';
import PIIsdk from '../src/index';

let toolset: PIIsdk;

beforeAll(() => {
  toolset = new PIIsdk({
    piiURL: 'fake',
    audience: 'fake',
    authURL: 'fake',
    clientId: 'fake',
    clientSecret: 'fake',
  });
});

test('creating key', async () => {
  const key = await toolset.createKey();
  expect(key).toBeDefined();
  expect(key).toBeInstanceOf(Object);
  expect(key).toHaveProperty('keys');
  expect(key.keys[0]).toBeDefined();
  expect(key.keys[0].privateKeyHex).toBeDefined();
});

test('encrypting+decrypting data from & to', async () => {
  const fromKey = await toolset.createKey();
  const toKey = await toolset.createKey();
  const fromDidKey = fromKey.did;
  const toDidKey = toKey.did;

  const data = 'dummy data';
  const edata = await toolset.encryptPII({
    senderDIDKey: fromDidKey,
    recipientDIDkeys: [toDidKey],
    body: data,
    keypair: fromKey,
  });
  const jdata = JSON.parse(edata);

  expect(jdata).toHaveProperty('recipients');
  expect(jdata.recipients).toHaveLength(2);
  expect(
    jdata.recipients.find((x: any) => x.header.kid.split('#')[0] === fromDidKey)
  ).toBeDefined();
  expect(
    jdata.recipients.find((x: any) => x.header.kid.split('#')[0] === toDidKey)
  ).toBeDefined();

  expect(
    await toolset.decryptPII({ keypair: fromKey, encryptedMessage: edata })
  ).toEqual(data);
  expect(
    await toolset.decryptPII({ keypair: toKey, encryptedMessage: edata })
  ).toEqual(data);
});

test('decryption with unknown key', async () => {
  const fromKey = await toolset.createKey();
  const toKey = await toolset.createKey();
  const fromDidKey = fromKey.did;

  const data = 'dummy data';
  const edata = await toolset.encryptPII({
    senderDIDKey: fromDidKey,
    recipientDIDkeys: [],
    body: data,
    keypair: fromKey,
  });
  const jdata = JSON.parse(edata);
  expect(jdata).toHaveProperty('recipients');
  expect(
    jdata.recipients.find((x: any) => x.header.kid.split('#')[0] === fromDidKey)
  ).toBeDefined();

  await expect(
    toolset.decryptPII({ keypair: toKey, encryptedMessage: edata })
  ).rejects.toThrowError(
    'unable to decrypt DIDComm message with any of the locally managed keys'
  );
});

test('encrypting+decrypting data from, middleman & to', async () => {
  const fromKey = await toolset.createKey();
  const middleKey = await toolset.createKey();
  const toKey = await toolset.createKey();
  const fromDidKey = fromKey.did;
  const middleDidKey = middleKey.did;
  const toDidKey = toKey.did;

  const data = 'dummy data';
  const edata = await toolset.encryptPII({
    senderDIDKey: fromDidKey,
    recipientDIDkeys: [toDidKey, middleDidKey],
    body: data,
    keypair: fromKey,
  });
  const jdata = JSON.parse(edata);
  expect(jdata).toHaveProperty('recipients');
  expect(
    jdata.recipients.find((x: any) => x.header.kid.split('#')[0] === fromDidKey)
  ).toBeDefined();
  expect(
    jdata.recipients.find((x: any) => x.header.kid.split('#')[0] === toDidKey)
  ).toBeDefined();
  expect(
    jdata.recipients.find(
      (x: any) => x.header.kid.split('#')[0] === middleDidKey
    )
  ).toBeDefined();

  expect(
    await toolset.decryptPII({ keypair: fromKey, encryptedMessage: edata })
  ).toEqual(data);
  expect(
    await toolset.decryptPII({ keypair: middleKey, encryptedMessage: edata })
  ).toEqual(data);
  expect(
    await toolset.decryptPII({ keypair: toKey, encryptedMessage: edata })
  ).toEqual(data);
});
