/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeAll, expect, test } from 'vitest';
import PIIsdk from '../src/index';

let toolset: PIIsdk;
beforeAll(() => {
  toolset = new PIIsdk({
    piiURL: 'fake',
    audience: 'fake',
    authURL: 'fake',
    clientId: 'fake',
    clientSecret: 'fake',
  });
});

const ivms = {
  originator: {
    originatorPersons: [
      {
        naturalPerson: {
          name: [
            {
              nameIdentifier: [
                {
                  primaryIdentifier: 'Frodo',
                  secondaryIdentifier: 'Baggins',
                  nameIdentifierType: 'LEGL',
                },
              ],
            },
          ],
          nationalIdentification: {
            nationalIdentifier: 'AABBCCDDEEFF0011223344',
            nationalIdentifierType: 'CCPT',
            countryOfIssue: 'NZ',
          },
          dateAndPlaceOfBirth: {
            dateOfBirth: '1900-01-01',
            placeOfBirth: 'Planet Earth',
          },
          geographicAddress: [
            {
              addressLine: ['Cool Road /-.st'],
              country: 'BE',
              addressType: 'HOME',
            },
          ],
        },
      },
    ],
    accountNumber: ['01234567890'],
  },
  beneficiary: {
    beneficiaryPersons: [
      {
        naturalPerson: {
          name: [
            {
              nameIdentifier: [
                {
                  primaryIdentifier: 'Bilbo',
                  secondaryIdentifier: 'Bolson',
                  nameIdentifierType: 'LEGL',
                },
              ],
            },
          ],
        },
      },
    ],
    accountNumber: ['01234567890'],
  },
};

test('flatten & unflatten', async () => {
  const flat = toolset.flattenPII(ivms);
  const unflat = toolset.unflattenPII(flat);
  expect(unflat).toEqual(ivms);
});

test('flatten & unflatten', async () => {
  const ivms = {
    a: 1, // should be excluded by flattenPII
    b: '2',
    c: [3], // should be excluded by flattenPII
    d: ['4'],
    e: [{ f: '5' }],
    i: '', // should be excluded by flattenPII
    l: true, // should be excluded by flattenPII
    m: false, // should be excluded by flattenPII

    g: null, // should be excluded by flattenPII
    h: undefined, // should be excluded by flattenPII
    j: [], // should be excluded by flattenPII
    k: {}, // should be excluded by flattenPII
    /* eslint-disable */
    n: () => {}, // should be excluded by flattenPII
    o: Symbol('test'), // should be excluded by flattenPII
  };
  const flat = toolset.flattenPII(ivms);
  const unflat = toolset.unflattenPII(flat);
  expect(unflat).not.toEqual(ivms);
  expect(flat.length).toEqual(3);
  expect(flat.find((x: any) => x.key === 'b')).toBeDefined();
  expect(flat.find((x: any) => x.key === 'd[0]')).toBeDefined();
  expect(flat.find((x: any) => x.key === 'e[0].f')).toBeDefined();
});
