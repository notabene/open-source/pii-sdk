/* eslint-disable @typescript-eslint/no-explicit-any */
import { describe, expect, test } from 'vitest';
import { Ecdh1PuXC20P } from '../src';

describe('Ecdh1PU XC20P', () => {
  const agent = new Ecdh1PuXC20P();

  test('veramo backward compat.: decrypting data encrypted by veramo', async () => {
    const fromKey = {
      did: 'did:key:z6Mki6n5rch5LUkEoLK8YsfMvQH59vrNcnrTRYTNxJYu7Syo',
      controllerKeyId:
        '362f84172fbab5b69aea0adc18c5612c47dd47eeaefa85e17da0e2c73a6238b2',
      keys: [
        {
          type: 'Ed25519',
          kid: '362f84172fbab5b69aea0adc18c5612c47dd47eeaefa85e17da0e2c73a6238b2',
          publicKeyHex:
            '362f84172fbab5b69aea0adc18c5612c47dd47eeaefa85e17da0e2c73a6238b2',
          meta: { algorithms: ['Ed25519', 'EdDSA'] },
          kms: 'local',
          privateKeyHex:
            '797a4ca95d0fda4e2f70bde52f3b1f5e8335f55106202780080c62948c4ca779362f84172fbab5b69aea0adc18c5612c47dd47eeaefa85e17da0e2c73a6238b2',
        },
      ],
      services: [],
      provider: 'did:key',
    };

    const emsg =
      '{"protected":"eyJ0eXAiOiJhcHBsaWNhdGlvbi9kaWRjb21tLWVuY3J5cHRlZCtqc29uIiwic2tpZCI6ImRpZDprZXk6ejZNa2k2bjVyY2g1TFVrRW9MSzhZc2ZNdlFINTl2ck5jbnJUUllUTnhKWXU3U3lvI3o2TFNnMU12ZnlzWDJrSlpNMXBTSGhCUXVZZnhTemdrOGRWbWFRcmJTVHlweXd1TCIsImVuYyI6IlhDMjBQIn0","iv":"0tzi83y57H5Xl5kz7Ed9-95lwtfijVQX","ciphertext":"YWNOgotseah1O2xAborbBCpaXNU0yAthhQG3pAsnyIWwUvnhwF6fMhrbB3CrAMDBxCy4OXTO5L-WH4NSN-GzJU6ybEgbZeLlaGjmoJ416AXOYKjpkK_tArftX6Zqx-P4iJujEgjIEsQWOQ_KPO33ddYeKEyFbEM7uAFMFuKRFiYAX_FM4TsHkfYNwlTkRTifldziRimIoBRsbyFqsdM7ziH0RRWcpddErS_ZsSDFNhQRGzf6lBh2x4KZJqdOfBl6cWgwj1nvuOgZzI9hVohc9JUSCy-tIwBmuORAbnJuzXclAdtF","tag":"72oXocXubwYwhYRIAaWGIQ","recipients":[{"encrypted_key":"IcSna1NMoO4BilB6hnq9NM1NkSFN7d2gVE1m7LU31d8","header":{"alg":"ECDH-1PU+XC20PKW","iv":"ouWruolFxdGcdk6viWzroj7IsFT9D-WJ","tag":"kxXKliTFIH8NVLGNBOMbjw","epk":{"kty":"OKP","crv":"X25519","x":"Do8AM_rc6OdBZ_qvE4H0tZ4VWskecfkjw05uHY9x8g4"},"kid":"did:key:z6Mki6n5rch5LUkEoLK8YsfMvQH59vrNcnrTRYTNxJYu7Syo#z6LSg1MvfysX2kJZM1pSHhBQuYfxSzgk8dVmaQrbSTypywuL"}}]}';

    const msg1 = await agent.decrypt(
      JSON.parse(emsg),
      fromKey.keys[0].privateKeyHex
    );

    expect(msg1.body === 'x11111111');
  });

  test('basic encrypt + decrypt flow', async () => {
    const fromKey = await agent.createKey();
    const toKey = await agent.createKey();

    const body = 'demo test';
    const emsg = await agent.encrypt({
      message: {
        type: 'notabene.escrow.pii.message',
        from: fromKey.did,
        to: fromKey.did,
        id: 'notabene.escriw.pii-' + new Date().getTime(),
        body,
      },
      bcc: [toKey.did],
      senderPrivateKeyHex: fromKey.keys[0].privateKeyHex,
    });

    const msg1 = await agent.decrypt(
      JSON.parse(emsg),
      fromKey.keys[0].privateKeyHex
    );
    const msg2 = await agent.decrypt(
      JSON.parse(emsg),
      toKey.keys[0].privateKeyHex
    );

    expect((body === msg1.body) === msg2.body);
  });

  test('basic 2 encrypt + decrypt flow', async () => {
    const fromKey = await agent.createKey();
    const toKey = await agent.createKey();

    const body = 'demo test';
    const emsg = await agent.encrypt({
      message: {
        type: 'notabene.escrow.pii.message',
        from: fromKey.did,
        to: toKey.did,
        id: 'notabene.escriw.pii-' + new Date().getTime(),
        body,
      },
      bcc: [],
      senderPrivateKeyHex: fromKey.keys[0].privateKeyHex,
    });

    const msg1 = await agent.decrypt(
      JSON.parse(emsg),
      fromKey.keys[0].privateKeyHex
    );
    const msg2 = await agent.decrypt(
      JSON.parse(emsg),
      toKey.keys[0].privateKeyHex
    );

    expect((body === msg1.body) === msg2.body);
  });

  test('failing encrypt + decrypt flow', async () => {
    const fromKey = await agent.createKey();
    const toKey = await agent.createKey();

    const body = 'demo test';
    const emsg = await agent.encrypt({
      message: {
        type: 'notabene.escrow.pii.message',
        from: fromKey.did,
        to: fromKey.did,
        id: 'notabene.escriw.pii-' + new Date().getTime(),
        body,
      },
      bcc: [],
      senderPrivateKeyHex: fromKey.keys[0].privateKeyHex,
    });

    await expect(
      agent.decrypt(JSON.parse(emsg), toKey.keys[0].privateKeyHex)
    ).rejects.toThrowError(
      'unable to decrypt DIDComm message with any of the locally managed keys'
    );
  });
});
