/* eslint-disable @typescript-eslint/no-explicit-any */
import { beforeAll, expect, test, vi } from 'vitest';
import PIIsdk from '../src/index';

vi.mock('axios');
vi.mock('../src/utils/create-client');
let toolset: PIIsdk;
beforeAll(() => {
  toolset = new PIIsdk({
    piiURL: 'fake',
    audience: 'fake',
    authURL: 'fake',
    clientId: 'fake',
    clientSecret: 'fake',
  });
  toolset.http = { get: vi.fn(), post: vi.fn() } as any;
});

test('test successful get & post', async () => {
  toolset.http.get = vi.fn().mockResolvedValueOnce({
    data: { key: 'name', value: 'John' },
  });

  toolset.http.post = vi.fn().mockResolvedValueOnce([
    {
      key: 'name',
      value: 'QmPPYDvAup99hgM7xYuCEdtZeqtJT5JiBjwZ3sGXePPP8G',
    },
  ]);

  const put = await toolset.putPII(
    'http://localhost:8000',
    'did:ethr:0x000',
    'did:ethr:0x111',
    [{ key: 'name', value: 'John' }]
  );
  expect(put[0].value).toEqual(
    'QmPPYDvAup99hgM7xYuCEdtZeqtJT5JiBjwZ3sGXePPP8G'
  );

  const get = await toolset.getPIIfromURL(put[0].value);
  expect(get.data.key).toEqual('name');
  expect(get.data.value).toEqual('John');
});

test('test failed get & post', async () => {
  toolset.http.get = vi.fn().mockRejectedValueOnce(new Error());

  toolset.http.post = vi.fn().mockRejectedValueOnce(new Error());

  await expect(
    toolset.putPII(
      'http://localhost:8000',
      'did:ethr:0x8b8b8b8b8b8b8b8b8b8b8b',
      'did:ethr:0x8b8b8b8b8b8b8b8b8b8b8b',
      [{ key: 'name', value: 'John' }]
    )
  ).rejects.toThrow();

  await expect(
    toolset.getPIIfromURL('http://localhost:8000/get?cid=fake')
  ).rejects.toThrow();
});
