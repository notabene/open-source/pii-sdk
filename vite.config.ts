/// <reference types="vitest" />
import { defineConfig } from 'vite';

export default defineConfig({
  test: {
    globals: true,
    environment: 'node',
    exclude: [
      '.trunk',
      '.husky',
      '.parcel-cache',
      '.vscode',
      '.yarn',
      'dist',
      'node_modules',
      '.git',
      'src',
    ],
  },
});
