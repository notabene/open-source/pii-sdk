/* eslint-disable @typescript-eslint/no-explicit-any */
import { JWE } from 'did-jwt';

export interface ICryptography {
  createKey: () => Promise<IKeyPair>;
  encrypt: (args: IPackArgs) => Promise<string>;
  decrypt: (
    message: JWE,
    recipientPrivateKeyHex: string
  ) => Promise<IDIDCommMessage>;
}

export interface IPackArgs {
  message: IDIDCommMessage;
  bcc: string[];
  senderPrivateKeyHex: string;
}

export interface IDIDCommMessage {
  type: string;
  from: string;
  to?: string;
  id: string;
  body: any;
}

export interface IKey {
  type: string;
  kid: string;
  publicKeyHex: string;
  meta: { algorithms: string[] };
  kms: string;
  privateKeyHex: string;
}

export interface IKeyPair {
  did: string;
  controllerKeyId: string;
  keys: IKey[];
  services: [];
  provider: 'did:key';
}
