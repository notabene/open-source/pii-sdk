/* eslint-disable @typescript-eslint/no-explicit-any */
import { arrayify, hexlify } from '@ethersproject/bytes';
import { ed25519 } from '@noble/curves/ed25519';
import {
  convertPublicKeyToX25519,
  convertSecretKeyToX25519,
} from '@stablelib/ed25519';
import base64url from 'base64url';
import bs58 from 'bs58';
import {
  createJWE,
  Decrypter,
  decryptJWE,
  Encrypter,
  JWE,
  xc20pAuthDecrypterEcdh1PuV3x25519WithXc20PkwV2,
  xc20pAuthEncrypterEcdh1PuV3x25519WithXc20PkwV2,
} from 'did-jwt';
import Multibase from 'multibase';
import Multicodec from 'multicodec';
import { DecryptError, MissingVariableError } from '../errors';
import { ICryptography, IDIDCommMessage, IKeyPair, IPackArgs } from './types';

function decodeJoseBlob(blob: string) {
  return JSON.parse(Buffer.from(base64url.decode(blob), 'utf-8').toString());
}

function convertPublicKeyToX25519Kid(key: string) {
  const myKeyBytes = convertPublicKeyToX25519(arrayify('0x' + key));
  const hexKey = hexlify(myKeyBytes).substring(2);
  return Buffer.from(
    Multibase.encode(
      'base58btc',
      Multicodec.addPrefix('x25519-pub', Buffer.from(hexKey, 'hex'))
    )
  ).toString();
}

async function computeRecipient(didkey: string): Promise<IRecipient> {
  // decode did:key public key

  didkey = didkey.split('#')[0]; // sanitize potential anchors
  const publicKeyBytes = bs58.decode(didkey.replace('did:key:z', '')).slice(2);
  const publicKeyHex = Buffer.from(publicKeyBytes).toString('hex');
  const kid = didkey + '#' + convertPublicKeyToX25519Kid(publicKeyHex);

  return {
    kid,
    publicKeyBytes,
    publicKeyHex,
  };
}
interface IRecipient {
  kid: string;
  publicKeyBytes: Uint8Array;
  publicKeyHex: string;
}
export class Ecdh1PuXC20P implements ICryptography {
  async createKey(): Promise<IKeyPair> {
    // We replaced "@transmute/did-key-ed25519" by "@noble/curves/ed25519", below is the did:key generation part
    const priv = ed25519.utils.randomPrivateKey();
    const privateKeyHex = Buffer.from(priv).toString('hex');
    const pub = ed25519.getPublicKey(priv);
    const publicKeyHex = Buffer.from(pub).toString('hex');

    const buffer = new Uint8Array(2 + pub.length);
    buffer[0] = 237; // ED25519_MULTICODEC_IDENTIFIER;
    buffer[1] = 1; // VARIABLE_INTEGER_TRAILING_BYTE;
    buffer.set(pub, 2);
    const fingerprint = `z${bs58.encode(buffer)}`;
    const did = `did:key:${fingerprint}`;

    return {
      did,
      controllerKeyId: publicKeyHex,
      keys: [
        {
          type: 'Ed25519',
          kid: publicKeyHex,
          publicKeyHex,
          meta: { algorithms: ['Ed25519', 'EdDSA'] },
          kms: 'local',
          privateKeyHex: privateKeyHex + publicKeyHex, // backward-compatible syntax
        },
      ],
      services: [],
      provider: 'did:key',
    };
  }

  async encrypt(args: IPackArgs): Promise<string> {
    if (!args?.message?.from) {
      throw new MissingVariableError(`"from"`);
    }

    // 2: compute recipients
    const recipients: IRecipient[] = [];
    const sender = await computeRecipient(args.message.from);
    recipients.push(sender);
    if (args.message.to)
      recipients.push(await computeRecipient(args.message.to));
    for (const to of args.bcc) {
      recipients.push(await computeRecipient(to));
    }

    // 3. create Encrypter for each recipient
    const encrypters: Encrypter[] = recipients
      .map((recipient) => {
        {
          return xc20pAuthEncrypterEcdh1PuV3x25519WithXc20PkwV2(
            convertPublicKeyToX25519(recipient.publicKeyBytes),
            convertSecretKeyToX25519(
              Buffer.from(args.senderPrivateKeyHex, 'hex')
            ),
            { kid: recipient.kid }
          );
        }
      })
      .filter((x) => !!x);

    const protectedHeader = {
      skid: sender.kid,
      typ: 'application/didcomm-encrypted+json',
    };

    // 4. createJWE
    const messageBytes = Buffer.from(JSON.stringify(args.message), 'utf-8');
    const jwe = await createJWE(messageBytes, encrypters, protectedHeader);
    const message = JSON.stringify(jwe);
    return message;
  }

  async decrypt(
    jwe: JWE,
    recipientPrivateKeyHex: string
  ): Promise<IDIDCommMessage> {
    const protectedHeader = decodeJoseBlob(jwe.protected);
    const isender = await computeRecipient(protectedHeader.skid);

    for (const recipient of jwe.recipients || []) {
      if (!recipient.header.kid)
        throw new MissingVariableError('recipient missing header kid value');

      const decrypter: Decrypter =
        xc20pAuthDecrypterEcdh1PuV3x25519WithXc20PkwV2(
          convertSecretKeyToX25519(Buffer.from(recipientPrivateKeyHex, 'hex')),
          convertPublicKeyToX25519(isender.publicKeyBytes)
        );

      try {
        const decryptedBytes = await decryptJWE(jwe, decrypter);
        const decryptedMsg = Buffer.from(decryptedBytes).toString();
        const message = JSON.parse(decryptedMsg);
        return message;
      } catch (err) {
        // no-op
      }
    }

    throw new DecryptError(
      'unable to decrypt DIDComm message with any of the locally managed keys'
    );
  }
}
