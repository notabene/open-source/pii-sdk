export class NotabenePIISDKError extends Error {
  constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}

export class GetEscrowKeyError extends NotabenePIISDKError {
  constructor() {
    super('Failed to obtain EscrowKeyPair');
  }
}

export class MissingPrivateKeyError extends NotabenePIISDKError {
  constructor() {
    super('Missing "privateKeyHex" in provded "keypair"');
  }
}

export class MissingVariableError extends NotabenePIISDKError {
  constructor(detail: string) {
    super('Missing Input Variable: ' + detail);
  }
}

export class DecryptError extends NotabenePIISDKError {
  constructor(detail: string) {
    super(detail);
  }
}
