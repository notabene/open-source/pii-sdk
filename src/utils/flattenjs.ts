/* eslint-disable @typescript-eslint/no-explicit-any */
import _ from 'lodash';

// Maintain previous exported func names
export function undo(params: any) {
  return inflate(params);
}

export function inflate(params: any) {
  return _.reduce(
    params,
    function (result: any, value: any, key: any) {
      return _.set(result, key, value);
    },
    {}
  );
}

// Maintain previous exported func names
export function convert(obj: any, preserveEmpty = false) {
  return flatten(obj, preserveEmpty);
}

export function flatten(obj: any, preserveEmpty = false) {
  return _.transform(
    obj,
    function (result: any, value: any, key: any) {
      if (_.isObject(value)) {
        const flatMap = _.mapKeys(
          flatten(value, preserveEmpty),
          function (_mvalue: any, mkey: any) {
            if (_.isArray(value)) {
              const index = mkey.indexOf('.');
              if (-1 !== index) {
                return `${key}[${mkey.slice(0, index)}]${mkey.slice(index)}`;
              }
              return `${key}[${mkey}]`;
            }
            return `${key}.${mkey}`;
          }
        );

        _.assign(result, flatMap);

        // Preverve Empty arrays and objects
        if (preserveEmpty && _.keys(flatMap).length === 0) {
          result[key] = value;
        }
      } else {
        result[key] = value;
      }

      return result;
    },
    {}
  );
}
