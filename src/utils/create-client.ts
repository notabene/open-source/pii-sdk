/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from 'axios';
import oauth from 'axios-oauth-client';
import tokenProvider from 'axios-token-interceptor';
import { Config } from '../index';
import freezeSys from './freeze-object';

const tokenInterceptor = (config: Config) => {
  return () =>
    getToken(
      config.clientId as string,
      config.clientSecret as string,
      config.authURL,
      config.audience
    );
};

export const getToken = async (
  clientId: string,
  clientSecret: string,
  authURL = 'https://auth.notabene.id',
  audience = 'https://pii.notabene.id'
) => {
  const resp = await axios.post(
    `${authURL}/oauth/token`,
    {
      grant_type: 'client_credentials',
      client_id: clientId,
      client_secret: clientSecret,
      audience: audience,
    },
    {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    }
  );
  return resp.data;
};

const errorInterceptor = (error: any) => {
  return Promise.reject({
    req: {
      url: '' + error?.response?.config?.baseURL + error?.response?.config?.url,
      method: error?.request?.method,
    },
    status: error?.response?.status,
    statusText: error?.response?.statusText,
    err: JSON.stringify(
      error?.response?.data?.err || error?.response?.data || error
    ),
  });
};

axios.interceptors.request.use(
  (res) => res,
  (error) => errorInterceptor(error)
);
axios.interceptors.response.use(
  (res) => res,
  (error) => errorInterceptor(error)
);

export function createClient(config: Config) {
  const client = axios.create({
    headers: {
      'X-Notabene-SDK': 'pii-sdk',
    },
  });

  client.interceptors.request.use(
    (res) => res,
    (error) => errorInterceptor(error)
  );
  client.interceptors.response.use(
    (res) => res,
    (error) => errorInterceptor(error)
  );

  client.interceptors.response.use((response) => {
    return freezeSys(response.data);
  });

  client.interceptors.request.use(
    oauth.interceptor(tokenProvider, tokenInterceptor(config))
  );

  return client;
}
