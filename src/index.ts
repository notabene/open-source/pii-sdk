/* eslint-disable @typescript-eslint/no-explicit-any */
import { AxiosInstance } from 'axios';
import { Ecdh1PuXC20P } from './cryptography';
import { ICryptography, IKeyPair } from './cryptography/types';
import {
  GetEscrowKeyError,
  MissingPrivateKeyError,
  MissingVariableError,
} from './errors';
import { createClient } from './utils/create-client';
import { flatten, inflate } from './utils/flattenjs';
export * from './cryptography';

export enum PIIEncryptionMethod {
  HOSTED,
  END_2_END,
  HYBRID,
}

export type Config = {
  piiURL?: string | undefined;
  clientId?: string | undefined;
  clientSecret?: string | undefined;
  audience?: string | undefined;
  authURL?: string | undefined;
};

const defaults: Partial<Config> = {
  piiURL: 'https://pii.notabene.id',
  audience: 'https://pii.notabene.id',
  authURL: 'https://auth.notabene.id',
};

export default class PIIsdk {
  http: AxiosInstance;
  config: Config;
  agent: ICryptography;

  constructor(config: Config) {
    const configWithDefaults = { ...defaults, ...config };
    this.config = configWithDefaults;
    this.http = createClient(this.config);
    this.agent = new Ecdh1PuXC20P();
  }

  async createKey() {
    return this.agent.createKey();
  }

  async getEscrowDIDkey(vaspDID: string) {
    if (!this.config.piiURL) throw new MissingVariableError('NOTABENE_PII_URL');
    const resp = (await this.http.get(
      this.config.piiURL + '/get-escrow-didkey?vaspDID=' + vaspDID
    )) as any;
    return resp;
  }

  async rotateEscrowDIDkey(vaspDID: string) {
    if (!this.config.piiURL) throw new MissingVariableError('NOTABENE_PII_URL');
    const resp = (await this.http.get(
      this.config.piiURL + '/rotate-escrow-didkey?vaspDID=' + vaspDID
    )) as any;
    return resp;
  }

  async encryptPII(input: {
    senderDIDKey: string;
    recipientDIDkeys: string[];
    body: string;
    keypair: IKeyPair;
  }): Promise<string> {
    const { senderDIDKey, keypair, body, recipientDIDkeys } = input;

    const message = {
      type: 'notabene.escrow.pii.message',
      from: senderDIDKey,
      to: undefined,
      id: 'notabene.escriw.pii-' + new Date().getTime(),
      body: body,
    };

    const senderPrivateKeyHex: string | undefined =
      keypair?.keys?.[0]?.privateKeyHex;
    if (!senderPrivateKeyHex) throw new MissingPrivateKeyError();

    const emsg = await this.agent.encrypt({
      message,
      bcc: recipientDIDkeys,
      senderPrivateKeyHex,
    });

    return emsg;
  }

  async decryptPII(input: {
    encryptedMessage: string;
    keypair: IKeyPair;
  }): Promise<string> {
    const senderPrivateKeyHex: string | undefined =
      input.keypair?.keys?.[0]?.privateKeyHex;
    if (!senderPrivateKeyHex) throw new MissingPrivateKeyError();

    const dmsg = await this.agent.decrypt(
      JSON.parse(input.encryptedMessage),
      senderPrivateKeyHex
    );

    return dmsg.body;
  }

  flattenPII(data: any) {
    // flatten complex pii object to single object
    let ret = flatten(data, true);
    // convert object to array of key-value pairs
    ret = Object.keys(ret).map((key: string) => {
      return { key, value: ret[key] };
    });
    // remove non-string & empty string values
    ret = ret.filter((x: any) => {
      if (!x.key || typeof x.value !== 'string') return false;
      return typeof x.value === 'string' && x.value.length;
    });
    return ret;
  }

  unflattenPII(data: any) {
    // convert array of key-value pairs to single object
    const ret = data.reduce((acc: any, a: any) => {
      return { ...acc, [a.key]: a.value };
    }, {});
    // unflatten object
    return inflate(ret);
  }

  async putPII(
    piiURL: string,
    fromDID: string,
    toDID: string | undefined,
    data: { key: string; value: string }[]
  ) {
    const payload = {
      fromDID,
      toDID,
      data,
    };
    const resp = (await this.http.post(`${piiURL}/put`, payload)) as any;
    return resp;
  }

  async getPIIfromURL(pii_cid_url: string) {
    const resp = (await this.http.get(pii_cid_url)) as any;
    return resp;
  }

  async getPIIbyCIDs(cids: string[], piiURL: string): Promise<any> {
    if (!cids.length) return [];
    const resp = (await this.http.post(`${piiURL}/get`, cids)) as any;
    return resp;
  }

  async getPIIObject(
    pii: any,
    piiURL: string,
    keypair: IKeyPair
  ): Promise<any> {
    const flattenedPii = this.flattenPII(pii);
    const cids = flattenedPii.map((x: any) => x.value);
    const outArr = await this.getPIIbyCIDs(cids, piiURL);

    // replace CIDs with the decrypted data
    for (const item of outArr) {
      const kv = JSON.parse(item.data);
      const raw = await this.decryptPII({
        encryptedMessage: kv.value,
        keypair,
      });
      flattenedPii.find((x: any) => x.value === item.cid).value = raw;
    }

    return this.unflattenPII(flattenedPii);
  }

  async generatePIIField(input: {
    pii: any;
    originatorVASPdid: string | undefined;
    beneficiaryVASPdid: string | undefined;
    counterpartyDIDKey: string | undefined;
    keypair: IKeyPair;
    senderDIDKey: string;
    encryptionMethod: PIIEncryptionMethod;
  }) {
    if (!this.config.piiURL) throw new MissingVariableError('piiURL');

    const recipientDIDkeys: string[] = [];
    if (
      input.encryptionMethod === PIIEncryptionMethod.HYBRID &&
      (input.originatorVASPdid || input.beneficiaryVASPdid) // order matters
    ) {
      const escrowKey = (
        (await this.getEscrowDIDkey(
          (input.originatorVASPdid || input.beneficiaryVASPdid) as string // order matters
        )) as any
      ).did;
      if (!escrowKey) throw new GetEscrowKeyError();
      recipientDIDkeys.push(escrowKey);
    }

    if (input.counterpartyDIDKey) {
      recipientDIDkeys.push(input.counterpartyDIDKey);
    }

    const flatPII = this.flattenPII(input.pii);
    for (const obj of flatPII) {
      obj.value = await this.encryptPII({
        keypair: input.keypair,
        senderDIDKey: input.senderDIDKey,
        recipientDIDkeys,
        body: obj.value,
      });
    }

    const cids = await this.putPII(
      this.config.piiURL,
      (input.originatorVASPdid || input.beneficiaryVASPdid) as string, // order matters
      input.beneficiaryVASPdid || input.originatorVASPdid, // order matters
      flatPII
    );
    const piiIvms = this.unflattenPII(cids);
    return piiIvms;
  }
}
